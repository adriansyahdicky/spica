package com.sinarmas.spica;

import com.sinarmas.spica.constant.SpicaConstants;
import com.sinarmas.spica.dto.BinaryNodeDTO;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


@SpringBootTest
class SpicaEngineApplicationTests {

	@Test
	void contextLoads() {

		String testCase1 = MessageFormat.format("({0})", "!AdditionalInsured1HeightIsNotBlank || !AdditionalInsured1HeightIsNotZero");
		String testCase2 = MessageFormat.format("({0})", "AdditionalInsured1HasNotClaimCorporateHistory");
		String testCase3 = MessageFormat.format("({0})", "AdditionalInsured1HeightGreaterThanMinHeight && AdditionalInsured1HeightLessThanMaxHeight");
		String testCase4 = MessageFormat.format("({0})", "!InsuredGenderIsNotNull || InsuredGenderIsNotBlank");
		String testCase5 = MessageFormat.format("({0})", "^!SPAJIsGIO && (((InsuredLsbsIdIs212||InsuredLsbsIdIs227||InsuredLsbsIdIs223) && InsuredIsSarIsLessThan120000000)||((InsuredLsbsIdIs208||InsuredLsbsIdIs219) && InsuredIsSarIsLessThan100000000)||(InsuredLsbsIdIs73||InsuredLsbsIdIs130||InsuredLsbsIdIs144||InsuredLsbsIdIs143||InsuredLsbsIdIs175||InsuredLsbsIdIs203||InsuredLsbsIdIs142))");

		String replace1 = testCase1.replaceAll("\\s", "");
		String[] splitted1 = replace1.split
				("((?<=&&)|(?=&&)|(?=\\|\\|)|(?<=\\|\\|))|(?=\\()|(?<=\\()|(?=\\))|(?<=\\))|(?=!)|(?<=!)|(?=^)|(?<=^)");

		String replace2 = testCase2.replaceAll("\\s", "");
		String[] splitted2 = replace2.split
				("((?<=&&)|(?=&&)|(?=\\|\\|)|(?<=\\|\\|))|(?=\\()|(?<=\\()|(?=\\))|(?<=\\))|(?=!)|(?<=!)|(?=^)|(?<=^)");

		String replace3 = testCase3.replaceAll("\\s", "");
		String[] splitted3 = replace3.split
				("((?<=&&)|(?=&&)|(?=\\|\\|)|(?<=\\|\\|))|(?=\\()|(?<=\\()|(?=\\))|(?<=\\))|(?=!)|(?<=!)|(?=^)|(?<=^)");

		String replace4 = testCase4.replaceAll("\\s", "");
		String[] splitted4 = replace4.split
				("((?<=&&)|(?=&&)|(?=\\|\\|)|(?<=\\|\\|))|(?=\\()|(?<=\\()|(?=\\))|(?<=\\))|(?=!)|(?<=!)|(?=^)|(?<=^)");

		String replace5 = testCase5.replaceAll("\\s", "");
		String[] splitted5 = replace5.split
				("((?<=&&)|(?=&&)|(?=\\|\\|)|(?<=\\|\\|))|(?=\\()|(?<=\\()|(?=\\))|(?<=\\))|(?=!)|(?<=!)|(?=^)|(?<=^)");
	}

	@Test
	void testLength(){
		String parameter = "'DICKY KEREN'";
		int testZeroLength = parameter.length();
		String zeroSubstring = parameter.substring(1, testZeroLength - 1);
		System.out.println(zeroSubstring);
	}

	@Test
	void testPrerequisite() throws ScriptException {
		String prerequisiteExpression = "!true && true && (true || true) && !true";
		boolean result = (Boolean) new ScriptEngineManager().getEngineByName("javascript").
				eval(prerequisiteExpression);
		System.out.println(result);
	}

	@Test
	void testStoreSpicaForm(){
		List<Integer> spicaForm = new ArrayList<>();
		spicaForm.add(144);
		spicaForm.add(61);
		spicaForm.forEach(integer -> {
			SpicaConstants.StoreTopicFormSpica.
					formTopicSpicas.add(SpicaConstants.Topic.TOPIC_SPICA+integer);
		});
		System.out.println(SpicaConstants.StoreTopicFormSpica.formTopicSpicas.size());
	}
}
