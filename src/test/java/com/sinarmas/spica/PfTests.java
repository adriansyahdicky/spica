package com.sinarmas.spica;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

@Slf4j
@SpringBootTest
public class PfTests {

    @Test
    void invoke() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        String methodName = "NumberEquals";
        Class<?> cls = Class.forName("com.sinarmas.spica.pf.PF");
        Object _instance = cls.newInstance();
        Object[] objects = {new BigDecimal(278), new BigDecimal(279)};
        Class<?>[] params = new Class[objects.length];

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] instanceof BigDecimal) {
                params[i] = BigDecimal.class;
            } else if (objects[i] instanceof String) {
                params[i] = String.class;
            } else if (objects[i] instanceof Date) {
                params[i] = Date.class;
            } else if (objects[i] instanceof Boolean) {
                params[i] = Boolean.class;
            }
        }

        int existMethod =
                (int) Arrays.stream(cls.getMethods()).filter(method ->
                        method.getName().equalsIgnoreCase(methodName)).count();
        if(existMethod == 0){
            log.info("METHOD NOT EXISTS IN CLASS PF");
        }else {
            Method myMethod = cls.getDeclaredMethod(methodName, params);
            Object result = myMethod.invoke(_instance, objects);
            log.info("RESULT {} ", result);
        }
    }


}
