package com.sinarmas.spica.pf;


import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.chrono.GregorianChronology;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class PF {

    public static Boolean ValueIsNull(Object param) {
        return param == null;
    }

    public static Boolean IsGreaterThan(BigDecimal number1, BigDecimal number2) {
        Boolean result = null;
        if(number1 != null && number2 != null) {
            result = number1.compareTo(number1) > 0;
        } else {
            result = false;
        }
        return result;
    }

    public static Boolean NumberEquals(BigDecimal number1, BigDecimal number2) {

        if (number1 != null && number2 != null) {
            return number1.equals(number2);
        } else {
            return false;
        }
    }

    public static Boolean BooleanCompareOr(Boolean bool, Boolean bool2) {
        if(bool != null && bool2 != null)
            return bool || bool2;
        return false;
    }

    public static Boolean BooleanCompareAnd(Boolean bool, Boolean bool2) {
        if(bool !=null && bool2 != null)
            return bool && bool2;
        return false;
    }

    public static Date Today(){
        Date result;
        LocalDate localDate = LocalDate.now();
        result = java.sql.Date.valueOf(localDate);
        return result;
    }

    public static BigDecimal Square(BigDecimal number1) {
        BigDecimal result = null;
        if(number1 != null) {
            result = number1.multiply(number1);
        }
        return result;
    }

    public static BigDecimal Divide(BigDecimal number1, BigDecimal number2) {
        if(number1 != null && number2 != null)
            return number1.divide(number2);

        return null;
    }

    public static BigDecimal Multiply(BigDecimal number1, BigDecimal number2) {

        if(number1 != null && number2 != null)
            return number1.multiply(number2);

        return null;
    }

    public static BigDecimal Add(BigDecimal number1, BigDecimal number2) {
        BigDecimal result = null;
        if(number1 != null && number2 != null) {
            result = number1.add(number2);
        }
        return result;
    }

    public static BigDecimal Percentage(BigDecimal number1, BigDecimal number2) {
        BigDecimal result = null;
        if(number1 != null && number2 != null) {
            result = number1.divide(number2);
            result = result.multiply(new BigDecimal(100));
        }
        return result;
    }

    public static Integer DateDiff(Date date1, Date date2) {
        Integer result = null;
        if(date1 != null && date2 != null) {
            DateTime firstDate = new DateTime(date1, GregorianChronology.getInstance());
            DateTime secondDate = new DateTime(date2, GregorianChronology.getInstance());
            Days diffInDays = Days.daysBetween(secondDate, firstDate);
            result = diffInDays.getDays();
        }

        return result;
    }

    public static Boolean IsGreaterThanOrEquals(BigDecimal number1, BigDecimal number2) {
        if(number1 != null && number2 != null) {
            return number1.compareTo(number2) >= 0;
        } else {
            return false;
        }
    }

    public static Boolean IsLessThanOrEquals(BigDecimal number1, BigDecimal number2) {
        if(number1 != null && number2 != null) {
            return number1.compareTo(number2) <= 0;
        } else {
            return false;
        }
    }

    public static Boolean BooleanIsTrue(Boolean bool) {
        Boolean result = false;
        if(bool != null) {
            if(bool == true) {
                result = true;
            } else {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }
}
