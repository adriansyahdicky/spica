package com.sinarmas.spica.service;

import com.sinarmas.spica.dto.FieldDTO;
import com.sinarmas.spica.dto.FormDTO;
import com.sinarmas.spica.dto.RequestGetDataAdapterDTO;
import com.sinarmas.spica.repository.QueryNativeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class FormValidateService {
        @Autowired
        QueryNativeRepository queryNativeRepository;
        @Autowired
        ExecutionRulesService executionRulesService;

        public void formValidate(FormDTO formDTO, String regSpaj){

            if(formDTO.getId() != null){
                log.info("FORM SERVICE ALREADY UPDATE {} ", formDTO.getId());
                Map<String, Object> getDataAdapter =
                        queryNativeRepository.getDataAdapter(RequestGetDataAdapterDTO.builder()
                                .field(formDTO.getStrFields())
                                .source(formDTO.getSource())
                                .primaryAttribute(formDTO.getPrimaryAttribute())
                                .valuePrimaryAttribute(regSpaj)
                                .build());
                log .info("GET DATA ADAPTER {} ", getDataAdapter);
                getDataAdapter.forEach((s, o) -> {
                    Optional<FieldDTO> fieldDTOValue = formDTO.getFields().stream().filter(fieldDTO ->
                            fieldDTO.getName().equalsIgnoreCase(s)).findFirst();
                    fieldDTOValue.ifPresent(fieldDTO -> fieldDTO.setValue(o));
                });
            }

             //call invoke for variable
            Map<String, Object> fieldAndPutVariable = executionRulesService.executeVariable(formDTO);
            log.info("RESULT VARIABLE INVOKE {} ", fieldAndPutVariable);

//            //call invoke for variable
            Map<String, Object> fieldAndPutRules = executionRulesService.executeRules(formDTO, fieldAndPutVariable);
            log.info("RESULT RULE INVOKE {} ", fieldAndPutRules);

    }
}
