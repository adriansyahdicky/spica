package com.sinarmas.spica.service;

import com.sinarmas.spica.exception.NotFoundException;
import com.sinarmas.spica.model.LstSpicaForm;
import com.sinarmas.spica.repository.LstSpicaFormRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class LstSpicaFormService {

    @Autowired
    private LstSpicaFormRepository lstSpicaFormRepository;

    public LstSpicaForm getById(Integer id){
        return lstSpicaFormRepository.findByLspcFormIdAndLspcFormFlagActive(id, 1).
                orElseThrow(() -> new NotFoundException("Unable to find SPICA Form with Id "+ id));
    }

    public List<LstSpicaForm> getListForm(){
        return lstSpicaFormRepository.findAllByLspcFormFlagActive(1);
    }

}
