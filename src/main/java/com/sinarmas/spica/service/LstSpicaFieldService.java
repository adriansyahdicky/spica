package com.sinarmas.spica.service;

import com.sinarmas.spica.dto.FieldDTO;
import com.sinarmas.spica.exception.BusinessException;
import com.sinarmas.spica.model.LstSpicaField;
import com.sinarmas.spica.repository.QueryNativeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class LstSpicaFieldService {

    @Autowired
    private QueryNativeRepository queryNativeRepository;

    public List<LstSpicaField> getAllByFormId(Integer formId){
        return queryNativeRepository.getSpicaFields(formId);
    }

    public FieldDTO validationSpicaField(LstSpicaField field) {

            FieldDTO fieldDTO = new FieldDTO();
            if (field.getLspcFieldName().split(String.valueOf(":',.#".toCharArray())).length > 1)
                throw new BusinessException("Syntax error variable " + field.getLspcFieldName() + " Field name should not contain special character.");
            fieldDTO.setName(field.getLspcFieldName());

            switch (field.getLspcFieldType().toLowerCase().trim()) {
                case "text":
                case "char":
                case "clob":
                case "varchar":
                case "varchar2":
                    fieldDTO.setType("text");
                    break;
                case "number":
                case "integer":
                case "long":
                    fieldDTO.setType("number");
                    break;
                case "boolean":
                    fieldDTO.setType("boolean");
                    break;
                case "date":
                    fieldDTO.setType("date");
                    break;
                default:
                    throw new BusinessException("Syntax error variable : " +
                            field.getLspcFieldName() + "Unable to recognize data type ." + field.getLspcFieldType());
            }

            switch (field.getLspcFieldNullable().toLowerCase().trim()) {
                case "y":
                case "yes":
                case "true":
                    fieldDTO.setNullable(Boolean.TRUE);
                    break;
                case "n":
                case "no":
                case "false":
                    fieldDTO.setNullable(Boolean.FALSE);
                    break;
                default:
                    throw new BusinessException("Syntax error variable :" + field.getLspcFieldName() + " Unable to nullable " + field.getLspcFieldNullable());
            }
            fieldDTO.setFormId(field.getLspcFormId());
            return fieldDTO;

    }

}
