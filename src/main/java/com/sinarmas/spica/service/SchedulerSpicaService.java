package com.sinarmas.spica.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SchedulerSpicaService {

    @Autowired
    private FormService formService;
    @Autowired
    private SpicaEngineServices spicaEngineServices;

    @Scheduled(fixedRate = 5000)
    public void spicaEngine(){
        try{
            log.info("START SPICA ENGINE ----------------------------");
            spicaEngineServices.runSpicaEngine();
        }catch (Exception ex){
            log.error("error spica engine {} ", ex.getMessage());
        }
    }

}
