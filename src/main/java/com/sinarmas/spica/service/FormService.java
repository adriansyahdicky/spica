package com.sinarmas.spica.service;

import com.sinarmas.spica.dto.*;
import com.sinarmas.spica.exception.BusinessException;
import com.sinarmas.spica.model.*;
import com.sinarmas.spica.repository.QueryNativeRepository;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Service
public class FormService {

    @Autowired
    private LstSpicaFormService lstSpicaFormService;
    @Autowired
    private LstSpicaFieldService lstSpicaFieldService;
    @Autowired
    private LstSpicaVariableService lstSpicaVariableService;
    @Autowired
    private QueryNativeRepository queryNativeRepository;
    @Autowired
    private LstSpicaConditionService lstSpicaConditionService;
    @Autowired
    private LstSpicaRuleService lstSpicaRuleService;
    @Autowired
    private ExecutionRulesService executionRulesService;

    public FormDTO initialize(Integer formId){
        FormDTO formDTO = new FormDTO();
        try {
            //Form Initiation
            LstSpicaForm lstSpicaForm = lstSpicaFormService.getById(formId);
            log.info("LST SPICA FORM {} ", new Gson().toJson(lstSpicaForm));
            formDTO.setId(lstSpicaForm.getLspcFormId());
            formDTO.setDescription(lstSpicaForm.getLspcFormDescription());
            formDTO.setType(lstSpicaForm.getLspcFormType());
            formDTO.setPrimaryAttribute(lstSpicaForm.getLspcFormPrimaryAttribute());
            formDTO.setSource(lstSpicaForm.getLspcFormSource());

            //Field Initiation
            List<LstSpicaField> lstSpicaFields = lstSpicaFieldService
                    .getAllByFormId(formId);
            log.info("LST_SPICA_FIELD SIZE {}", lstSpicaFields.size());
            List<String> stringFields = new ArrayList<>();
                    lstSpicaFields.forEach(field -> {
                formDTO.getFields().add(lstSpicaFieldService.validationSpicaField(field));
                stringFields.add(field.getLspcFieldName());
            });
            log.info("FORM DTO SPICA FIELDS {} ", formDTO.getFields().size());
            String strFields = StringUtils.join(stringFields, ", ");
            strFields.substring(0, strFields.length() - 1);
            log.info("LST SPICA AFTER JOIN  {} ", strFields);
            formDTO.setStrFields(strFields);

            //Variables Nodes Initiation
            List<LstSpicaVariable> lstSpicaVariables = lstSpicaVariableService.getAllByFormId(formId);
            log.info("LST SPICA VARIABLE SIZE {} ", lstSpicaVariables.size());
            List<VariableDTO> nodes = new ArrayList<>();
            lstSpicaVariables.forEach(lstSpicaVariable -> nodes.add(lstSpicaVariableService.
                validationSpicaVariableAndMapping(lstSpicaVariable)));
            log.info("LST SPICA VARIABLE NODES SIZE {} ", nodes.size());

            //Variables parameters Initiation
            List<LstSpicaPFParameter> spicaPFParameters =
                queryNativeRepository.getSpicaPFParameter();

            variableParameterInitiation(nodes, formDTO, spicaPFParameters);

            //Variables Edges Initiation
            List<String> edges = new ArrayList<>();
            nodes.forEach(variableDTO -> variableDTO.getParameters().forEach(o -> {
                if(o instanceof VariableDTO){
                    edges.add(String.format("%s:%s", variableDTO.getName(),
                        ((VariableDTO)o).getName()));
                }
            }));
            log.info("DATA EDGES {} ", edges);

            //Variables Sort (Topological Sort)
            List<VariableDTO> sorted = new ArrayList<>();
            variableSortTopologi(sorted, nodes, edges, formDTO);

            //Rules Initiation
            spicaRules(formId, formDTO, spicaPFParameters);
            log.info("SUCCESS INITIALIZE {} ", formDTO.getId());

        }catch (Exception ex){
            log.error("ERROR INITIALIZE SPICA {}", ex.getMessage());
            throw ex;
        }
        return formDTO;
    }

    private void variableParameterInitiation(List<VariableDTO> nodes,
                                             FormDTO formDTO,
                                             List<LstSpicaPFParameter> spicaPFParameters){
        nodes.forEach(variableDTO -> {
            for (int jCount = 0; jCount < variableDTO.getParameters().size(); jCount++){
                String parameter = (String)variableDTO.getParameters().get(jCount);
                String typeParameter;
                if(parameter.startsWith("F")){
                    FieldDTO find = this.Field(parameter.substring(1), formDTO);
                    if(find == null)
                        throw new BusinessException("Syntax error variable "+variableDTO.getName()+ " parameter :"+jCount+" Unable to find field "+parameter.substring(1));
                    variableDTO.getParameters().set(jCount, find);
                    typeParameter = find.getType();
                }else if(parameter.startsWith("V")){
                    VariableDTO find = null;
                    Optional<VariableDTO> dataNode = nodes.stream().filter(variableDTO1 ->
                            variableDTO1.getName().equalsIgnoreCase(parameter.substring(1))).findFirst();

                    if(dataNode.isPresent()){
                        find = dataNode.get();
                    }

                    if(find == null)
                        throw new BusinessException("Syntax error variable "+variableDTO.getName()+ " parameter :"+jCount+" Unable to find field "+parameter.substring(1));

                    variableDTO.getParameters().set(jCount, find);
                    typeParameter = find.getType();
                }else if (parameter.startsWith("'") && parameter.endsWith("'") && parameter.length() >= 2){
                    Object params = parameter.substring(1, parameter.length() - 2);
                    variableDTO.getParameters().set(jCount, params);
                    typeParameter = "text";
                }else if(parameter.startsWith("#")){
                    variableDTO.getParameters().set(jCount,  LocalDate.parse(parameter));
                    typeParameter = "date";
                }else{
                    variableDTO.getParameters().set(jCount, new BigDecimal(parameter));
                    typeParameter = "number";
                }

                if(validationPFParameterTypeVariable(
                        jCount,
                        variableDTO,
                        typeParameter,
                        spicaPFParameters,
                        parameter))
                    log.error("---------------------");
            }
        });
    }

    private FieldDTO Field(String name, FormDTO formDTO)
    {
        Optional<FieldDTO> fieldDTO = formDTO.getFields().stream().filter(fieldDTO1 -> fieldDTO1.getName().equalsIgnoreCase(name))
                        .findFirst();
        return fieldDTO.orElse(null);
    }

    private VariableDTO Variable(String name, FormDTO formDTO)
    {
        Optional<VariableDTO> variableDTO = formDTO.getVariables().stream().filter(variableDTO1 -> variableDTO1.getName().equalsIgnoreCase(name))
                        .findFirst();
        return variableDTO.orElse(null);
    }

    private void variableSortTopologi(List<VariableDTO> sorted, List<VariableDTO> nodes,
                                      List<String> edges, FormDTO formDTO){
        int index = -1;
        for (int iCount = 0; iCount < nodes.size() && index == -1; iCount++)
        {
            VariableDTO node = nodes.get(iCount);

            boolean incoming = false;
            for (int jCount = 0; jCount < edges.size() && !incoming; jCount++)
            {
                String edge = edges.get(jCount);
                if (edge.startsWith(String.format("%s:", node.getName()))) {
                    incoming = true;
                }
            }

            if (!incoming)
                index = iCount;
        }

        while (index >= 0){
            sorted.add(nodes.get(index));
            String variableName = nodes.get(index).getName();
            for (int iCount = edges.size() - 1; iCount >= 0; iCount--){
                if(edges.get(iCount).endsWith(String.format(":%s", variableName))){
                    edges.remove(iCount);
                }
            }
            nodes.remove(index);

            index = -1;
            for (int iCount = 0; iCount < nodes.size() && index == -1; iCount++){
                VariableDTO node = nodes.get(iCount);
                boolean incoming = false;
                for (int jCount = 0; jCount < edges.size() && !incoming; jCount++){
                    String edge = edges.get(jCount);
                    if (edge.startsWith(String.format("%s:", node.getName()))){
                        incoming=true;
                    }
                }

                if (!incoming)
                    index = iCount;
            }
        }

        if (edges.size() > 0)
            throw new BusinessException("Unable to sort variable because there is at least 1 cycle in variable setup.");

        formDTO.setVariables(sorted);
    }

    private void spicaRules(Integer formId, FormDTO formDTO, List<LstSpicaPFParameter> spicaPFParameters) {
        List<LstSpicaCondition> spicaConditions = queryNativeRepository.getSpicaConditions(formId);
        List<LstSpicaRule> spicaRules = queryNativeRepository.getSpicaRules(formId);
        RuleDTO[] ruleDTOS= new RuleDTO[spicaRules.size()];
        List<RuleDTO> ruleDTOList = Arrays.stream(ruleDTOS).collect(Collectors.toList());
        formDTO.setRules(ruleDTOList);

        List<LstSpicaPF> lstSpicaPFS = queryNativeRepository.getSpicaPF();
        for (int iCount = 0; iCount < spicaRules.size(); iCount++) {
            int finalICount = iCount;
            List<LstSpicaCondition> rows = spicaConditions.stream().filter(lstSpicaCondition ->
                    lstSpicaCondition.getLspcRuleName().
                            equalsIgnoreCase(spicaRules.get(finalICount).getLspcRuleName())).collect(Collectors.toList());
            ConditionDTO[] conditionDTOS = new ConditionDTO[rows.size()];
            List<ConditionDTO> conditions =Arrays.stream(conditionDTOS).collect(Collectors.toList());
            for (int jCount = 0; jCount < rows.size(); jCount++) {
                conditions.set(jCount, lstSpicaConditionService.mappingCondition(rows.get(jCount), lstSpicaPFS));

                for (int kCount = 0; kCount < conditions.get(jCount).getParameters().size(); kCount++) {
                    String parameter = (String) conditions.get(jCount).getParameters().get(kCount);
                    String typeParameter;
                    if (parameter.startsWith("F")) {
                        FieldDTO find = this.Field(parameter.substring(1), formDTO);
                        if (find == null)
                            throw new BusinessException("ERROR SPICA RULE FIELD ");
                        conditions.get(jCount).getParameters().set(kCount, find);
                        typeParameter = find.getType();
                    } else if (parameter.startsWith("V")) {
                        VariableDTO find = this.Variable(parameter.substring(1), formDTO);
                        if (find == null)
                            throw new BusinessException("ERROR SPICA RULE VARIABLE ");
                        conditions.get(jCount).getParameters().set(kCount, find);
                        typeParameter = find.getType();
                    } else if (parameter.startsWith("'") && parameter.endsWith("'") && parameter.length() >= 2) {
                        Object params = parameter.substring(1, parameter.length() - 1);
                        conditions.get(jCount).getParameters().set(kCount, params);
                        typeParameter = "text";
                    } else if (parameter.startsWith("#")) {
                        try {
                            conditions.get(jCount).getParameters().set(kCount, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                    .parse(parameter.substring(1)));
                            typeParameter = "date";
                        }catch (ParseException ex){
                            throw new RuntimeException(ex);
                        }
                    } else {
                        conditions.get(jCount).getParameters().set(kCount, BigDecimal.valueOf(Long.parseLong(parameter)));
                        typeParameter = "number";
                    }

                    if(validationPFParameterTypeCondition(jCount, kCount, conditions, typeParameter, spicaPFParameters, parameter))
                        log.error("---------------------");


                }
            }

            formDTO.getRules().set(iCount, lstSpicaRuleService.mappingRuleSpica(spicaRules.get(iCount), conditions));
        }
    }


    private boolean validationPFParameterTypeCondition(int jCount,
                                              int kCount,
                                              List<ConditionDTO> conditions,
                                              String typeParameter,
                                              List<LstSpicaPFParameter> spicaPFParameters,
                                              String parameter){
        List<LstSpicaPFParameter> parameters =
                spicaPFParameters.stream().filter(lstSpicaPFParameter ->
                        lstSpicaPFParameter.getLspcPfName().
                                equalsIgnoreCase(conditions.get(jCount).getFunctionName())).collect(Collectors.toList());

        Optional<LstSpicaPFParameter> spicaPFParameter = parameters.stream().filter(lstSpicaPFParameter ->
                        lstSpicaPFParameter.getLspcPfParameterId().equals(kCount + 1))
                .findFirst();

        if(spicaPFParameter.isPresent() && !checkPFParameterType(spicaPFParameter.get().getLspcPfParameterType(), typeParameter)
                .equalsIgnoreCase(typeParameter)) {
            log.error("ERROR PF PARAMETER NOT SAME PLEASE CHECK PF {} " +
                            "PARAMETER TYPE INDEX - {} " +
                            "ACTUAL PARAMETER IS {} AND PAREMETER OBJECT {} ",
                    conditions.get(jCount).getFunctionName(), spicaPFParameter.get().getLspcPfParameterId(),
                    spicaPFParameter.get().getLspcPfParameterType(),
                    parameter);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private boolean validationPFParameterTypeVariable(int jCount,
                                              VariableDTO variableDTO,
                                              String typeParameter,
                                              List<LstSpicaPFParameter> spicaPFParameters,
                                              String parameter){

        List<LstSpicaPFParameter> parameters =
                spicaPFParameters.stream().filter(lstSpicaPFParameter ->
                        lstSpicaPFParameter.getLspcPfName().
                                equalsIgnoreCase(variableDTO.getFunctionName())).collect(Collectors.toList());

        Optional<LstSpicaPFParameter> spicaPFParameter = parameters.stream().filter(lstSpicaPFParameter ->
                        lstSpicaPFParameter.getLspcPfParameterId().equals(jCount + 1))
                .findFirst();

        if(spicaPFParameter.isPresent() && !checkPFParameterType(spicaPFParameter.get().getLspcPfParameterType(), typeParameter)
                .equalsIgnoreCase(typeParameter)) {
            log.error("ERROR PF PARAMETER NOT SAME PLEASE CHECK PF {} " +
                            "PARAMETER TYPE INDEX - {} " +
                            "ACTUAL PARAMETER IS {} AND PAREMETER OBJECT {} ",
                    variableDTO.getFunctionName(),
                    spicaPFParameter.get().getLspcPfParameterId(),
                    spicaPFParameter.get().getLspcPfParameterType(),
                    parameter);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private String checkPFParameterType(String type, String finalType) {
        String returnType;
        switch (type.toLowerCase().trim()) {
            case "text":
            case "char":
            case "clob":
            case "varchar":
            case "varchar2":
                returnType = "text";
                break;
            case "number":
            case "integer":
            case "long":
                returnType = "number";
                break;
            case "boolean":
                returnType = "boolean";
                break;
            case "date":
                returnType = "date";
                break;
            default:
                returnType = finalType;
                break;
        }

        return returnType;
    }


}
