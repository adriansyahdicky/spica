package com.sinarmas.spica.service;

import com.sinarmas.spica.dto.VariableDTO;
import com.sinarmas.spica.exception.BusinessException;
import com.sinarmas.spica.model.LstSpicaVariable;
import com.sinarmas.spica.repository.QueryNativeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Slf4j
@Service
public class LstSpicaVariableService {

    @Autowired
    private QueryNativeRepository queryNativeRepository;

    public List<LstSpicaVariable> getAllByFormId(Integer formId){
        return queryNativeRepository.getSpicaVariables(formId);
    }

    public VariableDTO validationSpicaVariableAndMapping(LstSpicaVariable variable){
        VariableDTO variableDTO = new VariableDTO();
        variableDTO.setFormId(variable.getLspcFormId());
        if (variable.getLspcVariableName().split(String.valueOf(":',.#".toCharArray())).length > 1)
            throw new BusinessException("Syntax error variable " + variable.getLspcVariableName() + " Field name should not contain special character.");
        variableDTO.setName(variable.getLspcVariableName());

        switch (variable.getLspcVariableType().toLowerCase().trim())
        {
            case "text":
            case "char":
            case "clob":
            case "varchar":
            case "varchar2":
                variableDTO.setType("text");
                break;
            case "number":
            case "integer":
            case "long":
                variableDTO.setType("number");
                break;
            case "boolean":
                variableDTO.setType("boolean");
                break;
            case "date":
                variableDTO.setType("date");
                break;
            default:
                throw new BusinessException("Syntax error variable :"+variable.getLspcVariableName()+ " Unable to recognize data type "+variable.getLspcVariableType());
        }
        variableDTO.setExpression(variable.getLspcVariableExpression());
        String[] parts = variable.getLspcVariableExpression().split("\\(");
        String fn = parts[0].trim();

        if(parts.length != 2){
            throw new BusinessException("Syntax error variable : "+ variable.getLspcVariableName()+" There should be 1 character of '('");
        }else if(fn.length() == 0){
            throw new BusinessException("Syntax error variable : "+ variable.getLspcVariableName()+" Function name should not be empty ");
        }
        variableDTO.setFunctionName(fn);
        String[] parts2 = parts[1].split("\\)", 2);
        parts2[1] = ")";

        if(parts2.length != 2){
            throw new BusinessException("Syntax error variable : "+variable.getLspcVariableName()+ " There should be 1 character of ')' ");
        }

        if(!Objects.equals(parts2[0], "")) {
            String[] parts3 = parts2[0].split(String.valueOf(",".toCharArray()), -1);
            for (String s : parts3) {
                variableDTO.getParameters().add(s.trim());
            }
        }

        variableDTO.setDescription(variable.getLspcVariableDescription());
        return variableDTO;
    }

}
