package com.sinarmas.spica.service;

import com.google.gson.Gson;
import com.sinarmas.spica.dto.BinaryNodeDTO;
import com.sinarmas.spica.dto.ConditionDTO;
import com.sinarmas.spica.dto.RuleDTO;
import com.sinarmas.spica.exception.BusinessException;
import com.sinarmas.spica.model.LstSpicaRule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class LstSpicaRuleService {

    public RuleDTO mappingRuleSpica(LstSpicaRule lstSpicaRule, List<ConditionDTO> conditions){
        RuleDTO ruleDTO = new RuleDTO();
        ruleDTO.setFormId(lstSpicaRule.getLspcFormId());
        ruleDTO.setName(lstSpicaRule.getLspcRuleName());
        ruleDTO.setDescription(lstSpicaRule.getLspcRuleDescription());
        ruleDTO.setExpression(lstSpicaRule.getLspcRuleExpression());
        ruleDTO.setExpressionTree(parse(lstSpicaRule.getLspcRuleExpression()));
        ruleDTO.setErrorMessage(lstSpicaRule.getLspcRuleErrorMsg());
        ruleDTO.setPreRequisites(lstSpicaRule.getLspcRulePrerequisites());
        ruleDTO.setPreRequisitesTree(parse(lstSpicaRule.getLspcRulePrerequisites()));
        ruleDTO.setCategoryId(lstSpicaRule.getLspcRuleCategoryId());
        ruleDTO.setConditions(conditions);

        if (ruleDTO.getExpressionTree() != null)
            replaceCondition(ruleDTO.getExpressionTree(), conditions, lstSpicaRule);
        if (ruleDTO.getPreRequisitesTree() != null)
            replaceCondition (ruleDTO.getPreRequisitesTree(), conditions, lstSpicaRule);
        return ruleDTO;
    }

    private void replaceCondition(BinaryNodeDTO node, List<ConditionDTO> conditions, LstSpicaRule lstSpicaRule){
        if(node.getLeft() == null && node.getRight() == null){
            ConditionDTO conditionDTO = this.Condition((String)node.getValue(), conditions);
            if(conditionDTO == null)
                throw new BusinessException("Unable to find condition  "+ node.getValue() +" in rule "+ lstSpicaRule.getLspcRuleName());
            node.setValue(conditionDTO);
        }else{
            if(node.getLeft() != null)
                replaceCondition(node.getLeft(), conditions, lstSpicaRule);

            if(node.getRight() != null)
                replaceCondition(node.getRight(), conditions, lstSpicaRule);
        }
    }

    public ConditionDTO Condition(String name, List<ConditionDTO> conditions)
    {
        Optional<ConditionDTO> condition =
                conditions.stream().filter(conditionDTO -> conditionDTO.getName().equalsIgnoreCase(name))
                .findFirst();
        return condition.orElse(null);
    }

    private BinaryNodeDTO parse(String text){
        if(text == null)
            return null;

        List<String> parts = split(String.format("(%s)", text));
        Stack<String> operators = new Stack<>();
        Stack<BinaryNodeDTO> operands = new Stack<>();

        for (String part : parts){
            if (Objects.equals(part, "(") || Objects.equals(part, "!") || Objects.equals(part, "&&") || Objects.equals(part, "||") || Objects.equals(part, "^")) {
                operators.push(part);
            }else if(Objects.equals(part, ")")) {
                String op = operators.pop();
                while(!Objects.equals(op, "(")){
                    if (Objects.equals(op, "&&") || Objects.equals(op, "||") || Objects.equals(op, "^")){
                        BinaryNodeDTO node = new BinaryNodeDTO();
                        node.setValue(op);
                        node.setRight(operands.pop());
                        node.setLeft(operands.pop());
                        operands.push(node);
                    }
                    op = operators.pop();
                }
                while (operators.size() > 0 && Objects.equals(operators.peek(), "!"))
                {
                    BinaryNodeDTO negate = new BinaryNodeDTO();
                    negate.setValue(operators.pop());
                    negate.setLeft(operands.pop());
                    negate.setRight(null);
                    operands.push(negate);
                }
            }else{
                BinaryNodeDTO node = new BinaryNodeDTO();
                node.setValue(part);
                node.setLeft(null);
                node.setRight(null);
                while (Objects.equals(operators.peek(), "!"))
                {
                    BinaryNodeDTO negate = new BinaryNodeDTO();
                    negate.setValue(operators.pop());
                    negate.setLeft(node);
                    negate.setRight(null);
                    node = negate;
                }
                operands.push(node);
            }
        }
        if (operands.size() != 1 && operators.size() != 0)
            throw new BusinessException("Boolean expression does not satisfy expected grammar.");

        return operands.pop();
    }



    private List<String> split(String text){
        String data = text.replaceAll("\\s", "");
        String[] splitted = data.split
                ("((?<=&&)|(?=&&)|(?=\\|\\|)|(?<=\\|\\|))|(?=\\()|(?<=\\()|(?=\\))|(?<=\\))|(?=!)|(?<=!)|(?=^)|(?<=^)");
        return Arrays.stream(splitted).collect(Collectors.toList());
    }

}
