package com.sinarmas.spica.service;

import com.sinarmas.spica.dto.ConditionDTO;
import com.sinarmas.spica.exception.BusinessException;
import com.sinarmas.spica.model.LstSpicaCondition;
import com.sinarmas.spica.model.LstSpicaPF;
import com.sinarmas.spica.repository.QueryNativeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class LstSpicaConditionService {

    @Autowired
    private QueryNativeRepository queryNativeRepository;

    public ConditionDTO mappingCondition(LstSpicaCondition lstSpicaCondition, List<LstSpicaPF> lstSpicaPFS){

        ConditionDTO conditionDTO = new ConditionDTO();
        conditionDTO.setFormId(lstSpicaCondition.getLspcFormId());
        conditionDTO.setRule(lstSpicaCondition.getLspcRuleName());

        if (lstSpicaCondition.getLspcConditionName().split(String.valueOf(":',.# ".toCharArray())).length > 1)
            throw new BusinessException("Syntax error condition "+ lstSpicaCondition.getLspcConditionName()+": Condition name should not contain special character.");
        conditionDTO.setName(lstSpicaCondition.getLspcConditionName());
        conditionDTO.setExpression(lstSpicaCondition.getLspcConditionExpression());

        String[] parts = lstSpicaCondition.getLspcConditionExpression().split("\\(");
        String fn = parts[0].trim();

        String lspcPfName =
                lstSpicaPFS.stream().filter(lstSpicaPF -> lstSpicaPF.getLspcPfName().equalsIgnoreCase(fn))
                        .findFirst().map(LstSpicaPF::getLspcPfName).orElse(null);

        if (parts.length != 2)
            throw new BusinessException("Syntax error condition "+lstSpicaCondition.getLspcConditionName()+": There should be 1 character of '('");
        else if(fn.length() == 0)
            throw new BusinessException("Syntax error condition "+lstSpicaCondition.getLspcConditionName()+": Function name should not be empty.");
        else if(lspcPfName == null)
            throw new BusinessException("Syntax error condition "
                    +fn+ ": Unable to recognize function "+parts[0]);

        conditionDTO.setFunctionName(fn);

        String[] parts2 = parts[1].split("\\)", 2);
        parts2[1] = ")";

        if(parts2.length != 2)
            throw new BusinessException("Syntax error condition : "+lstSpicaCondition.getLspcConditionName()+ " There should be 1 character of ')' ");

        if(!Objects.equals(parts2[0], "")) {
            String[] parts3 = parts2[0].split(String.valueOf(",".toCharArray()), -1);
            for (String s : parts3) {
                conditionDTO.getParameters().add(s.trim());
            }
        }
        conditionDTO.setDescription(lstSpicaCondition.getLspcConditionDescription());

        return conditionDTO;
    }

}
