package com.sinarmas.spica.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinarmas.spica.config.KafkaConsumerConfig;
import com.sinarmas.spica.constant.SpicaConstants;
import com.sinarmas.spica.dto.FormDTO;
import com.sinarmas.spica.dto.ResponseSpicaPushDTO;
import com.sinarmas.spica.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Slf4j
@Service
public class SpicaEngineServices {

    @Autowired
    KafkaConsumerConfig kafkaConsumerConfig;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    FormService formService;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    FormValidateService formValidateService;

    private KafkaConsumer<String, Object> createConsumer() {
        KafkaConsumer<String, Object> consumer = kafkaConsumerConfig.consumerFactory();
        consumer.subscribe(SpicaConstants.StoreTopicFormSpica.formTopicSpicas);
        return consumer;
    }

    public void runSpicaEngine() {
        log.info("START RUN ENGINE SPICA .......................");

        KafkaConsumer<String, Object> consumer = createConsumer();

        ConsumerRecords<String, Object> consumerRecords =
                    consumer.poll(Duration.ofMillis(1000));

        consumerRecords.forEach(record -> {
            ResponseSpicaPushDTO responseSpicaPushDTO =
                    objectMapper.convertValue(record.value(), ResponseSpicaPushDTO.class);
            FormDTO formDTO;
            if(!redisUtils.hasKey(SpicaConstants.KeyRedis.KEY_STORE_FORM_ID+ responseSpicaPushDTO.getFormId())){
                formDTO = formService.initialize(responseSpicaPushDTO.getFormId());
                redisUtils.setValue(SpicaConstants.KeyRedis.KEY_STORE_FORM_ID+ responseSpicaPushDTO.getFormId(),
                        formDTO);
            }else{
                formDTO = (FormDTO) redisUtils.getValue(SpicaConstants.KeyRedis.KEY_STORE_FORM_ID+ responseSpicaPushDTO.getFormId());
            }

            if(formDTO != null)
                formValidateService.formValidate(formDTO, responseSpicaPushDTO.getRegSpaj());
        });

        consumer.commitAsync();
        log.info("DONE EXECUTE CONSUMER");
    }

}
