package com.sinarmas.spica.service;

import com.sinarmas.spica.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service
public class ExecutionRulesService {

    public Map<String, Object> executeVariable(FormDTO formDTO){
        Map<String, Object> fieldAndPutVariable = new HashMap<>();
        formDTO.getVariables().forEach(variableDTO -> {
            try {
                fieldAndPutVariable.put(variableDTO.getName(),
                        callInvoke(variableDTO.getFunctionName(),
                                variableDTO.getParameters().toArray(new Object[0]),
                                fieldAndPutVariable));
            } catch (InvocationTargetException | IllegalAccessException | ClassNotFoundException |
                     InstantiationException | NoSuchMethodException | ParseException e) {
                throw new RuntimeException(e);
            }
        });
        return fieldAndPutVariable;
    }

    public Map<String, Object> executeRules(FormDTO formDTO, Map<String, Object> fieldAndPutVariable){
        Map<String, Object> fieldAndPutRules = new HashMap<>();
        List<Object> values = new ArrayList<>();
        AtomicBoolean resultPrerequisites = new AtomicBoolean(false);
        AtomicReference<String> resultConcatComma = new AtomicReference<>();
        AtomicInteger j = new AtomicInteger();
        formDTO.getRules().forEach(ruleDTO -> {
            j.getAndIncrement();
            String prerequisite = null;
            if(StringUtils.isNotEmpty(ruleDTO.getPreRequisites())){
                prerequisite = ruleDTO.getPreRequisites().replaceAll("\\s", "");
                storeKeyValues(ruleDTO.getPreRequisitesTree(), values);
                String[] splitPrerequisites =  prerequisite.split("((?<=&&)|(?=&&)|(?=\\|\\|)|(?<=\\|\\|))|(?=\\()|(?<=\\()|(?=\\))|(?<=\\))|(?=!)|(?<=!)|(?=^)|(?<=^)");
                StringBuilder sb = new StringBuilder();

                for (int i=0; i<splitPrerequisites.length; i++) {

                    int finalI = i;
                    Optional<ConditionDTO> conditionDTO = values.stream().filter(o -> o instanceof ConditionDTO)
                            .map(o -> (ConditionDTO) o)
                            .filter(conditionDTO1 -> conditionDTO1.getName().trim().
                                    equalsIgnoreCase(splitPrerequisites[finalI].trim()))
                            .findFirst();

                    try {
                        if(conditionDTO.isPresent()) {
                            Object result = callInvoke(conditionDTO.get().getFunctionName(),
                                    conditionDTO.get().getParameters().toArray(new Object[0]), fieldAndPutVariable);
                            splitPrerequisites[i] = String.valueOf(result);
                        }
                    } catch (InvocationTargetException | IllegalAccessException | ClassNotFoundException |
                                 InstantiationException | NoSuchMethodException | ParseException e) {
                            throw new RuntimeException(e);
                    }

                    sb.append(splitPrerequisites[i]).append(" ");
                }
                 resultConcatComma.set(sb.toString());
                 fieldAndPutRules.put("RULE ACTIVATED-"+ j.get(), resultConcatComma.get());
                try {
                    if(StringUtils.isNotEmpty(resultConcatComma.get())) {
                        resultPrerequisites.set((Boolean) new ScriptEngineManager().getEngineByName("javascript").
                                eval(resultConcatComma.get()));
                        log.info("PREREQUISITES {} RESULT {} ", prerequisite, resultPrerequisites.get());
                    }
                } catch (ScriptException e) {
                    throw new RuntimeException(e);
                }
            }

            if(resultPrerequisites.get()) {
                ruleDTO.getConditions().forEach(conditionDTO -> {
                    try {
                        fieldAndPutRules.put(conditionDTO.getName(),
                                callInvoke(conditionDTO.getFunctionName(), conditionDTO.getParameters().toArray(new Object[0]), fieldAndPutVariable));
                    } catch (InvocationTargetException | IllegalAccessException | ClassNotFoundException |
                             InstantiationException | NoSuchMethodException | ParseException e) {
                        throw new RuntimeException(e);
                    }
                });
            }

        });

      return fieldAndPutRules;
    }

    public Object callInvoke(String methodName,
                                 Object[] objects,
                                 Map<String, Object> putVariable) throws InvocationTargetException, IllegalAccessException, ClassNotFoundException, InstantiationException, NoSuchMethodException, ParseException {

        for (int i = 0; i < objects.length; i++) {
            if(objects[i] instanceof VariableDTO){
                VariableDTO variableDTO = (VariableDTO) objects[i];
                if(putVariable.containsKey(variableDTO.getName()))
                    objects[i] = putVariable.get(variableDTO.getName()) == null ? null :
                            putVariable.get(variableDTO.getName());
            }

            if (objects[i] instanceof FieldDTO) {
                FieldDTO fieldDTO = (FieldDTO) objects[i];
                switch (fieldDTO.getType()) {
                    case "text":
                    case "char":
                    case "clob":
                    case "varchar":
                    case "varchar2":
                        objects[i] = fieldDTO.getValue();
                        break;
                    case "number":
                    case "integer":
                    case "long":
                        objects[i] = fieldDTO.getValue() == null ? null :
                                new BigDecimal(fieldDTO.getValue().toString());
                        break;
                    case "boolean":
                        objects[i] = fieldDTO.getValue() == null ? null :
                                Boolean.parseBoolean((String) fieldDTO.getValue());
                        break;
                    case "date":
                        objects[i] = fieldDTO.getValue() == null ? null :
                                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fieldDTO.getValue().toString());
                        break;
                }
            }
        }

        Class<?> cls = Class.forName("com.sinarmas.spica.pf.PF");
        Object _instance = cls.newInstance();
        Optional<Method> method1 = Arrays.stream(cls.getMethods()).filter(method ->
                        method.getName().equalsIgnoreCase(methodName))
                .findFirst();
        if(!method1.isPresent()){
//            throw new BusinessException("METHOD "+ methodName + " Not Exists In Class PF");
            return null;
        }
        Method myMethod = cls.getDeclaredMethod(methodName,  method1.get().getParameterTypes());
        return myMethod.invoke(_instance, objects);
    }

    public List<Object> storeKeyValues(BinaryNodeDTO root, List<Object> values) {
        treeTravel(root, values);
        return values;
    }

    private void treeTravel(BinaryNodeDTO node, List<Object> values) {
        if (node != null) {
            treeTravel(node.getLeft(), values);
            values.add(node.getValue());
            treeTravel(node.getRight(), values);
        }
    }

}
