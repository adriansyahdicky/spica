package com.sinarmas.spica.constant;


import java.util.List;

public class SpicaConstants {

    public static class Topic{
        public static String TOPIC_SPICA = "TOPIC-SPICA-";
    }

    public static class StoreTopicFormSpica{
        public static List<String> formTopicSpicas;
    }

    public static class KeyRedis{
        public static String KEY_STORE_FORM_ID = "FORM-ID-";
    }
}
