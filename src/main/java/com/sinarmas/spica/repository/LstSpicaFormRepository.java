package com.sinarmas.spica.repository;

import com.sinarmas.spica.model.LstSpicaForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LstSpicaFormRepository extends JpaRepository<LstSpicaForm, Integer> {
    Optional<LstSpicaForm> findByLspcFormIdAndLspcFormFlagActive(Integer id, int flagActive);
    List<LstSpicaForm> findAllByLspcFormFlagActive(int flagActive);
}
