package com.sinarmas.spica.repository;

import com.sinarmas.spica.dto.RequestGetDataAdapterDTO;
import com.sinarmas.spica.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Slf4j
@Repository
public class QueryNativeRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Map<String, Object> getDataAdapter(RequestGetDataAdapterDTO requestGetDataAdapterDTO){
        String SQL = "select "+ requestGetDataAdapterDTO.getField()+ " from "+
                requestGetDataAdapterDTO.getSource() +" where "+
                requestGetDataAdapterDTO.getPrimaryAttribute() + " = "+"'"+ requestGetDataAdapterDTO.getValuePrimaryAttribute()+"'";
        log.info("SQL QUERY {} ", SQL);
        return jdbcTemplate.queryForMap(SQL);
    }

    public List<LstSpicaField> getSpicaFields(Integer formId) {
        return jdbcTemplate.query(
                "select * from EKA.LST_SPICA_FIELD where LSPC_FORM_ID = ? AND LSPC_FIELD_FLAG_ACTIVE = 1",
                (rs, rowNum) ->
                        new LstSpicaField(
                                rs.getInt("LSPC_FORM_ID"),
                                rs.getString("LSPC_FIELD_NAME"),
                                rs.getString("LSPC_FIELD_TYPE"),
                                rs.getString("LSPC_FIELD_NULLABLE"),
                                rs.getInt("LSPC_FIELD_FLAG_ACTIVE")
                        ),
                formId);
    }

    public List<LstSpicaVariable> getSpicaVariables(Integer formId){
        return jdbcTemplate.query(
                "select * from EKA.LST_SPICA_VARIABLE where LSPC_FORM_ID = ? AND LSPC_VARIABLE_FLAG_ACTIVE = 1",
                (rs, rowNum) ->
                        new LstSpicaVariable(
                                rs.getInt("LSPC_FORM_ID"),
                                rs.getString("LSPC_VARIABLE_NAME"),
                                rs.getString("LSPC_VARIABLE_TYPE"),
                                rs.getString("LSPC_VARIABLE_EXPRESSION"),
                                rs.getString("LSPC_VARIABLE_DESCRIPTION"),
                                rs.getInt("LSPC_VARIABLE_FLAG_ACTIVE")
                        ),
        formId);
    }

    public List<LstSpicaCondition> getSpicaConditions(Integer formId){
        return jdbcTemplate.query(
                "select * from EKA.LST_SPICA_CONDITION where LSPC_FORM_ID = ? AND LSPC_CONDITION_FLAG_ACTIVE = 1",
                (rs, rowNum) ->
                        new LstSpicaCondition(
                                rs.getInt("LSPC_FORM_ID"),
                                rs.getString("LSPC_RULE_NAME"),
                                rs.getString("LSPC_CONDITION_NAME"),
                                rs.getString("LSPC_CONDITION_EXPRESSION"),
                                rs.getString("LSPC_CONDITION_DESCRIPTION"),
                                rs.getInt("LSPC_CONDITION_FLAG_ACTIVE")
                        ),
                formId);
    }

    public List<LstSpicaRule> getSpicaRules(Integer formId){
        return jdbcTemplate.query(
                "select * from EKA.LST_SPICA_RULE where LSPC_FORM_ID = ? AND LSPC_RULE_FLAG_ACTIVE = 1",
                (rs, rowNum) ->
                        new LstSpicaRule(
                                rs.getInt("LSPC_FORM_ID"),
                                rs.getString("LSPC_RULE_NAME"),
                                rs.getString("LSPC_RULE_DESCRIPTION"),
                                rs.getString("LSPC_RULE_EXPRESSION"),
                                rs.getString("LSPC_RULE_ERROR_MSG"),
                                rs.getInt("LSPC_RULE_FLAG_ACTIVE"),
                                rs.getString("LSPC_RULE_PREREQUISITES"),
                                rs.getInt("LSPC_RULE_CATEGORY_ID")
                        ),
                formId);
    }

    public List<LstSpicaPF> getSpicaPF(){
        return jdbcTemplate.query(
                "select * from EKA.LST_SPICA_PF WHERE LSPC_PF_FLAG_ACTIVE = 1",
                (rs, rowNum) ->
                        new LstSpicaPF(
                           rs.getString("LSPC_PF_NAME"),
                           rs.getString("LSPC_PF_TYPE"),
                           rs.getInt("LSPC_PF_FLAG_ACTIVE")
                        )
        );
    }

    public List<LstSpicaPFParameter> getSpicaPFParameter(){
        return jdbcTemplate.query(
                "select * from EKA.LST_SPICA_PF_PARAMETER",
                (rs, rowNum) ->
                        new LstSpicaPFParameter(
                                rs.getString("LSPC_PF_NAME"),
                                rs.getInt("LSPC_PF_PARAMETER_ID"),
                                rs.getString("LSPC_PF_PARAMETER_TYPE"),
                                rs.getString("LSPC_PF_PARAMETER_DESCRIPTION")
                        )
        );
    }
}
