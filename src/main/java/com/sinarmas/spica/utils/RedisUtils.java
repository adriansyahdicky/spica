package com.sinarmas.spica.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
@SuppressWarnings("squid:S112")
public class RedisUtils {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * Set cache expiration time
     *
     * @param key     key
     * @param timeout time
     * @return {Boolean}
     */
    public Boolean setExpire(final String key, final Duration timeout) {
        if (timeout.getSeconds() > 0) {
            return this.redisTemplate.expire(key, timeout.getSeconds(), TimeUnit.SECONDS);
        }
        return false;
    }

    /**
     * Get cache expiration time
     *
     * @param key key
     * @return time (seconds) 0 is permanently valid
     */
    public Long getExpire(final String key) {
        return this.redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * Does the key exist?
     *
     * @param key key
     * @return {Boolean}
     */
    public Boolean hasKey(final String key) {
        try {
            Boolean haskey = this.redisTemplate.hasKey(key);
            return haskey != null ? haskey : false;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Delete cache
     *
     * @param keys
     */
    public Boolean delete(final String... keys) {
        return keys.length
                == Optional.ofNullable(this.redisTemplate.delete(Arrays.asList(keys))).orElse(-1L);
    }

    // ============================String=============================

    /**
     * Get normal cache
     *
     * @param key key
     * @return value
     */
    public Object getValue(final String key) {
        return this.redisTemplate.opsForValue().get(key);
    }

    /**
     * Set normal cache
     *
     * @param key   key
     * @param value
     */
    public void setValue(final String key, final Object value) {
        this.redisTemplate.opsForValue().set(key, value);
    }

    /**
     * Set normal cache
     *
     * @param key     key
     * @param value
     * @param timeout time will be set indefinitely when it is less than or equal to 0
     */
    public void setValue(
            final String key, final Object value, final Duration timeout) {
        this.redisTemplate.opsForValue().set(key, value, timeout);
    }

    /**
     * Increasing
     *
     * @param key   key
     * @param delta how much to increase (greater than 0)
     * @return The value of key after adding the specified value
     */
    public Long incrementValue(final String key, final long delta) {
        if (delta > 0) {
            throw new RuntimeException("The increment factor must be greater than 0");
        }
        return this.redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * Decrease
     *
     * @param key   key
     * @param delta to be reduced by a few (less than 0)
     * @return reduces the value of key after the specified value
     */
    public Long decrementValue(final String key, final long delta) {
        if (delta < 0) {
            throw new RuntimeException("Decrease factor must be greater than 0");
        }
        return this.redisTemplate.opsForValue().increment(key, -delta);
    }

    // ================================Map=================================

    /**
     * HashGet
     *
     * @param key  key
     * @param item item
     * @return value
     */
    public Object getHash(final String key, final String item) {
        return this.redisTemplate.opsForHash().get(key, item);
    }

    /**
     * Get all the key values corresponding to hashKey
     *
     * @param key key
     * @return corresponding to multiple key values
     */
    public Map<Object, Object> getHash(final String key) {
        return this.redisTemplate.opsForHash().entries(key);
    }

    /**
     * HashSet
     *
     * @param key key
     * @param map corresponds to multiple key values
     */
    public void putHash(final String key, final Map<String, Object> map) {
        this.redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * HashSet and set the time
     *
     * @param key     key
     * @param map     corresponds to multiple key values
     * @param timeout time
     */
    public void putHash(
            final String key,
            final Map<String, Object> map,
            final Duration timeout) {
        this.redisTemplate.opsForHash().putAll(key, map);
        this.setExpire(key, timeout);
    }

    /**
     * Put data into a hash table, if it does not exist, it will be created
     *
     * @param key   key
     * @param item  item
     * @param value
     */
    public void putHash(
            final String key, final String item,  final Object value) {
        this.redisTemplate.opsForHash().put(key, item, value);
    }

    /**
     * Put data into a hash table, if it does not exist, it will be created
     *
     * @param key     key
     * @param item    item
     * @param value
     * @param timeout time Note: If the existing hash table has time, the original time will be replaced here
     */
    public void putHash(
            final String key,
            final String item,
            final Object value,
            final Duration timeout) {
        this.redisTemplate.opsForHash().put(key, item, value);
        this.setExpire(key, timeout);
    }

    /**
     * Delete the value in the hash table
     *
     * @param key  key
     * @param item item
     */
    public void deleteHash(final String key, final Object... item) {
        this.redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * Determine whether there is a value in the hash table
     *
     * @param key  key
     * @param item item
     * @return {Boolean}
     */
    public Boolean hasKeyHash(final String key, final String item) {
        return this.redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash increment If it does not exist, it will create one and return the newly added value
     *
     * @param key  key
     * @param item item
     * @param by   how many to increase (greater than 0)
     * @return The value of key after adding the specified value
     */
    public Double incrementHash(
            final String key, final String item, final double by) {
        return this.redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash decreasing
     *
     * @param key  key
     * @param item item
     * @param by   should be reduced (less than 0)
     * @return reduces the value of key after the specified value
     */
    public Double decrementHash(
            final String key, final String item, final double by) {
        return this.redisTemplate.opsForHash().increment(key, item, -by);
    }

    // ============================set=============================

    /**
     * Get all the values in the Set according to the key
     *
     * @param key key
     * @return Set<Object>
     */
    public Set<Object> getSet(final String key) {
        return this.redisTemplate.opsForSet().members(key);
    }

    /**
     * Query from a set according to value, whether it exists
     *
     * @param key   key
     * @param value
     * @return {Boolean}
     */
    public Boolean hasKeySet(final String key, final Object value) {
        return this.redisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * Put the data into the set cache
     *
     * @param key    key
     * @param values
     * @return put the number
     */
    public Long addSet(final String key, final Object... values) {
        return this.redisTemplate.opsForSet().add(key, values);
    }

    /**
     * Put the set data into the cache
     *
     * @param key     key
     * @param timeout time
     * @param values
     * @return put the number
     */
    public Long addSet(
            final String key,
            final Duration timeout,
            final Object... values) {
        final Long num = this.redisTemplate.opsForSet().add(key, values);
        this.setExpire(key, timeout);
        return num;
    }

    /**
     * Get the length of the set cache
     *
     * @param key key
     * @return the length of the cache
     */
    public Long getSetSize(final String key) {
        return this.redisTemplate.opsForSet().size(key);
    }

    /**
     * Remove the value
     *
     * @param key    key
     * @param values
     * @return remove the number
     */
    public Long removeSet(final String key, final Object... values) {
        return this.redisTemplate.opsForSet().remove(key, values);
    }
    // ===============================list=================================

    /**
     * Get the contents of the list cache
     *
     * @param key   key
     * @param start start
     * @param end   end 0 to -1 represent all values
     * @return list cached content
     */
    public List<Object> getList(
            final String key, final Long start, final Long end) {
        return this.redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * Get the length of the list cache
     *
     * @param key key
     * @return list buffer length
     */
    public Long getListSize(final String key) {
        return this.redisTemplate.opsForList().size(key);
    }

    /**
     * Get the value in the list by index
     *
     * @param key   key
     * @param index index>=0, 0 header, 1 second element, and so on; when index<0, -1, end of the table, -2 penultimate element, and so on
     * @return the value in the list
     */
    public Object getListIndex(final String key, final Long index) {
        return this.redisTemplate.opsForList().index(key, index);
    }

    /**
     * Put the list into the cache
     *
     * @param key   key
     * @param value
     * @return put the number
     */
    public Long pushList(final String key, final Object value) {
        return this.redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * Put the list into the cache
     *
     * @param key     key
     * @param value
     * @param timeout time
     */
    public Long pushList(
            final String key, final Object value, final Duration timeout) {
        final Long num = this.redisTemplate.opsForList().rightPush(key, value);
        this.setExpire(key, timeout);
        return num;
    }

    /**
     * Put the list into the cache
     *
     * @param key   key
     * @param value
     * @return put the number
     */
    public Long pushList(final String key, final List<Object> value) {
        return this.redisTemplate.opsForList().rightPushAll(key, value);
    }

    /**
     * Put the list into the cache
     *
     * @param key     key
     * @param value
     * @param timeout time
     * @return put the number
     */
    public Long pushList(
            final String key,
            final List<Object> value,
            final Duration timeout) {
        final Long num = this.redisTemplate.opsForList().rightPushAll(key, value);
        this.setExpire(key, timeout);
        return num;
    }

    /**
     * Modify a piece of data in the list according to the index
     *
     * @param key   key
     * @param index index
     * @param value
     */
    public void updateListIndex(
            final String key, final Long index, final Object value) {
        this.redisTemplate.opsForList().set(key, index, value);
    }

    /**
     * Remove N values as value
     *
     * @param key   key
     * @param count how many to remove
     * @param value
     * @return remove the number
     */
    public Long removeList(
            final String key,  final Long count,  final Object value) {
        return this.redisTemplate.opsForList().remove(key, count, value);
    }


}
