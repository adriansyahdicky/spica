package com.sinarmas.spica.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestInitiationDTO {
    private Integer formId;
    private String regSpaj;
}
