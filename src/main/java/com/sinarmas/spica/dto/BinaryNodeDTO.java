package com.sinarmas.spica.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BinaryNodeDTO {
    private Object value;
    private BinaryNodeDTO left;
    private BinaryNodeDTO right;
}
