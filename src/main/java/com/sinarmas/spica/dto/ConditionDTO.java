package com.sinarmas.spica.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConditionDTO {
    private Integer formId;
    private String rule;
    private String name;
    private String expression;
    private String functionName;
    private List<Object> parameters = new ArrayList<>();
    private String description;
}
