package com.sinarmas.spica.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestGetDataAdapterDTO {
    private String source;
    private String field;
    private String primaryAttribute;
    private String valuePrimaryAttribute;
}
