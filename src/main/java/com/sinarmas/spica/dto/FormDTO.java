package com.sinarmas.spica.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FormDTO {
    private Integer id;
    private String type;
    private String description;
    private String source;
    private String primaryAttribute;
    private List<FieldDTO> fields = new ArrayList<>();
    private List<VariableDTO> variables = new ArrayList<>();
    private List<RuleDTO> rules = new ArrayList<>();
    private String strFields;
}
