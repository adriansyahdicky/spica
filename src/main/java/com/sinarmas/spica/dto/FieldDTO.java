package com.sinarmas.spica.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FieldDTO {
    private Integer formId;
    private String name;
    private String type;
    private Boolean nullable;
    private Object value;

}
