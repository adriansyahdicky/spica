package com.sinarmas.spica.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RuleDTO {
    private Integer formId;
    private String name;
    private String description;
    private String expression;
    private BinaryNodeDTO expressionTree;
    private String errorMessage;
    private String preRequisites;
    private BinaryNodeDTO preRequisitesTree;
    private List<ConditionDTO> conditions = new ArrayList<>();
    private Integer categoryId;
}
