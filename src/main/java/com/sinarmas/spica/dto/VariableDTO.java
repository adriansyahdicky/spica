package com.sinarmas.spica.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VariableDTO {
    private Integer formId;
    private String name;
    private String type;
    private String expression;
    private String functionName;
    private List<Object> parameters = new ArrayList<>();
    private String description;
    private Object result;
}
