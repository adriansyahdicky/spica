package com.sinarmas.spica.dto;

import lombok.*;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseApplication<T> {
    private LocalDateTime timeResult;
    private T result;

    public static <T> ResponseApplication<T> result(@Nullable T result) {
        return ResponseApplication.<T>builder()
                .timeResult(LocalDateTime.now())
                .result(result)
                .build();
    }
}
