package com.sinarmas.spica;

import com.sinarmas.spica.constant.SpicaConstants;
import com.sinarmas.spica.service.LstSpicaFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

@EnableScheduling
@SpringBootApplication
public class SpicaEngineApplication {

	@Autowired
	private LstSpicaFormService lstSpicaFormService;

	public static void main(String[] args) {
		SpringApplication.run(SpicaEngineApplication.class, args);
	}

	@PostConstruct
	public void postConstructFormTopicSpica(){
		lstSpicaFormService.getListForm().forEach(lstSpicaForm -> SpicaConstants.StoreTopicFormSpica.
				formTopicSpicas.add(SpicaConstants.Topic.TOPIC_SPICA+lstSpicaForm.getLspcFormId()));
	}

}
