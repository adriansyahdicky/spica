package com.sinarmas.spica.exception;

import com.sinarmas.spica.dto.ResponseApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> notFoundException(Exception ex) {
        ResponseApplication<Object>
                errors = ResponseApplication.builder().timeResult(LocalDateTime.now())
                .result(ex.getMessage()).build();
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> businessException(Exception ex) {
        ResponseApplication<Object>
                errors = ResponseApplication.builder().timeResult(LocalDateTime.now())
                .result(ex.getMessage()).build();
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

}
