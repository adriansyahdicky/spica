package com.sinarmas.spica.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LstSpicaRule implements Serializable {
    private Integer lspcFormId;
    private String lspcRuleName;
    private String lspcRuleDescription;
    private String lspcRuleExpression;
    private String lspcRuleErrorMsg;
    private Integer lspcRuleFlagActive;
    private String lspcRulePrerequisites;
    private Integer lspcRuleCategoryId;
}
