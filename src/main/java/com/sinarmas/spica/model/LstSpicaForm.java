package com.sinarmas.spica.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "LST_SPICA_FORM")
public class LstSpicaForm {

    @Id
    @Column(name = "LSPC_FORM_ID")
    private Integer lspcFormId;

    @Column(name = "LSPC_FORM_TYPE")
    private String lspcFormType;

    @Column(name = "LSPC_FORM_DESCRIPTION")
    private String lspcFormDescription;

    @Column(name = "LSPC_FORM_SOURCE")
    private String lspcFormSource;

    @Column(name = "LSPC_FORM_PRIMARY_ATTRIBUTE")
    private String lspcFormPrimaryAttribute;

    @Column(name = "LSPC_FORM_FLAG_ACTIVE")
    private Integer lspcFormFlagActive;
}
