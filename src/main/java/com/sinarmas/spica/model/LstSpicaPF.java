package com.sinarmas.spica.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LstSpicaPF implements Serializable {
    private String lspcPfName;
    private String lspcPfType;
    private Integer lspcPfFlagActive;
}
