package com.sinarmas.spica.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LstSpicaField implements Serializable{

    private Integer lspcFormId;

    private String lspcFieldName;

    private String lspcFieldType;

    private String lspcFieldNullable;

    private Integer lspcFieldFlagActive;
}
