package com.sinarmas.spica.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LstSpicaPFParameter {
    private String lspcPfName;
    private Integer lspcPfParameterId;
    private String lspcPfParameterType;
    private String lspcPfParameterDescription;
}
