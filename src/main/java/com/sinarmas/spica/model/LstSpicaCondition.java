package com.sinarmas.spica.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LstSpicaCondition implements Serializable {
    private Integer lspcFormId;
    private String lspcRuleName;
    private String lspcConditionName;
    private String lspcConditionExpression;
    private String lspcConditionDescription;
    private Integer lspcConditionFlagActive;
}
