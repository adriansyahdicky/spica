package com.sinarmas.spica.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LstSpicaVariable {
    private Integer lspcFormId;

    private String lspcVariableName;

    private String lspcVariableType;

    private String lspcVariableExpression;

    private String lspcVariableDescription;

    private Integer lspcVariableFlagActive;
}
